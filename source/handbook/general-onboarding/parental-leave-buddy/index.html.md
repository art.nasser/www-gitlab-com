---
layout: markdown_page
title: "Parental Leave Reentry Buddies"
---

## Parental Leave Reentry Buddies

Fellow GitLab team members, regardless of whether they are a parent, are encouraged to volunteer as a Parental Leave Reentry Buddy. 

At a high level, a Parental Leave Reentry Buddy volunteers to help team members coming back online after [parental leave](/handbook/benefits/#parental-leave). It is recommended that this buddy work within the same department as the team member reentering work. With this in mind, please work with your manager to agree on selecting an appropriate buddy. 

If you are asked to be a buddy, work with your manager to delegate or temporarily pause certain duties to assist a team member as they ramp back up to 100%. 

As a guide, Parental Leave Reentry Buddies should set aside around 20% of their time to assist with reentry for two to four weeks. 

Responsibilities include:

1. Facilitate [coffee chats](/company/culture/all-remote/informal-communication/#coffee-chats) to catch up with team members on non-work topics.
1. Answer questions about top concerns, projects, and milestones that occur during parental leave.
1. Surface relevant handbook pages that are created during parental leave. 
1. Advise of relevant new hires onboarded during parental leave. 

*Specifics on responsibilities and expectations are still being considered, and will be added to this section in future iterations.* 

## Find a Parental Leave Reentry Buddy

Visit the [GitLab Team page](/company/team/) and search for 'Parental Leave Reentry Buddy', or ask on Slack in `#intheparenthood`.

## Become a Parental Leave Reentry Buddy

If you're comfortable using Git and GitLab and want to help team members troubleshoot problems and accelerate their learning, please follow these steps to indicate your availability as a `Parental Leave Reentry buddy`: 

1. Find your entry in the [team.yml](/handbook/git-page-update/#11-add-yourself-to-the-team-page) file.
1. Add `Parental Leave Reentry buddy` to the `departments` section in your entry (keeping your existing departments):
   ```
   departments:
     - ...
     - Parental Leave Reentry buddy
   ```
1. Add the following code above the `story` section in your entry:
   ```
     expertise:  |
                 <li><a href="/handbook/general-onboarding/parental-leave-buddy/">Parental Leave Reentry buddy</a></li>
   ```
1. If you already have an `expertise` section, add the list item portion of the above code:
   ```
                 <li><a href="/handbook/general-onboarding/parental-leave-buddy/">Parental Leave Reentry buddy</a></li>
   ```

